<?php

/**
* Action.php
*
* @category CB_Framework
* @package CB_Framework
* @copyright CYBRiDGE
* @license CYBRiDGE License 1.0
* @since PHP 5.0
* @link http://www.cybridge.jp/
*/

class NI_Action extends CB_Action{

	

	// 共通初期処理 init()より先に呼び出される。
	public function commonInit(){
		
	}
	public function auth(){
		
	}
	public function init(){
		
		
		
	}

	public function getFiles($name) {
		$arr = array();

		// キー一覧取得
		if($_FILES[$name]) {
			foreach( (array) $_FILES[$name]["name"] as $key => $name_s) {
				$arr[$key] = array();
			}

			// 中身を入れる
			foreach( (array) $arr as $key => $val) {
				$arr[$key]["name"]		= $_FILES[$name]["name"][$key];
				$arr[$key]["type"]		= $_FILES[$name]["type"][$key];
				$arr[$key]["tmp_name"]	= $_FILES[$name]["tmp_name"][$key];
				$arr[$key]["error"]		= $_FILES[$name]["error"][$key];
				$arr[$key]["size"]		= $_FILES[$name]["size"][$key];
			}
		}

		return $arr;
	}
	function getFileName($value) {
		$tmp_s = explode("/", $value);
		$file_name = $tmp_s[count($tmp_s)-1];
		return $file_name;
	}
	public function image_upload_tmp($file) {
		$result = array();

		$html_path			= "/img/sys/tmp/";
		$upload_dir			= CB_PUBLIC . $html_path;
		$allow_file_type	= array('jpg', 'jpeg', 'gif', 'png');
		$max_file_size		= 3000000000;
		$up					= new CB_SimpleFile($upload_dir, $allow_file_type, $max_file_size);
		$up->_debug			= false;

		// 拡張子取得
		$tmp_ext			= explode(".", $file["name"]);
		$tmp_index			= count($tmp_ext) - 1;
		$ext				= $tmp_ext[$tmp_index];
		$file_name			= md5(uniqid(rand(),1)) . "." . $ext;
		
				// 画像アップロード
		try {
			// エラー
			if(!$up->setFile($file, $file_name)){
				$result["error"] = 1;
			}
		} catch(Exception $e) {
			$result["error"] = 1;
			d(e);
		}

		// 権限変更
		@chmod($upload_dir.$file_name, 0777);

		$result["html_path"]	= $html_path . $file_name;
		$result["sys_path"]		= $upload_dir . $file_name;

		return $result;
	}
	function tmp_image_save($file_name, $save_dir, $width=100, $height=100, $is_delete=false,$new_name=null) {


		$tmp_s = explode("/", $file_name);
		$file_name = $tmp_s[count($tmp_s)-1];
		// ファイルがあればリサイズして保存
		if($file_name && file_exists(IMAGE_TMP_FULLPATH.$file_name)) {

			$imageEditor = new CB_ImageEditor();
			$imageEditor->load(IMAGE_TMP_FULLPATH.$file_name);
			$imageEditor->resize($width, $height);
			if(!is_null($new_name)){
				$file_name = $new_name;
			}
			$imageEditor->save($save_dir . $file_name, NULL);

			// 権限変更
			
			@chmod(IMAGE_TMP_FULLPATH.$file_name, 0777);
			
			

			// 削除
			if($is_delete) {
				unlink(IMAGE_TMP_FULLPATH.$file_name);
			}
		}

		return $file_name;
	}

	public function displayErrorActiveLink() {
		$this->setTemplate("_common/error_active.html");
	}

	
	public function displayErrorDelete($message=NULL) {
		$this->setTemplate("_common/error_delete.html");
	}

	public function displayErrorEmail($message=NULL) {
		$this->setTemplate("_common/error_email.html");
	}

	public function displayNotEmail($message=NULL) {
		$this->setTemplate("_common/error_facebook_account.html");
	}
	public function displayMaintenance($message=NULL) {
		$this->setTemplate("_common/maintenance.html");
	}

	/**
	*	ｃｂsendmail が使えないためｐｅａｒを使用
	*	開発環境と本番で使用するメールライブラリを変更
	*/

	public function sendMail($to, $from = MAIL_SUPPORT, $subject, $body = array()) {
		
		// development
		//---------------------------------------------
		$pattern = "/(.cb$|.cba$|cbri.net)/";
		if (preg_match($pattern, $_SERVER["HTTP_HOST"])) {
			
			$mail = new CB_Zend_Mail();

			if ($body) {

				// テンプレートの内容アサイン
				foreach ( (array) $body["data"] as $key => $val) {

					$mail->assign($key, $val);
				}

				//tpl 設定
				$tpl = $mail->fetch($body["tpl"]);
			}

			$mail->setBodyText($tpl)
				->setFrom($from)
				->addTo($to)
				->setSubject($subject)
				->send();


		// production
		//---------------------------------------------
		} else {

			//外部メールサーバー用設定
			$config = array(
				"host"		=> MAIL_SERVER_NAME,   // SMTPサーバー名
				"port"		=> FOREIGN_MAIL_PORT,              // ポート番号
				"auth"		=> true,            // SMTP認証を使用する
				"username"	=> FOREIGN_MAIL_USERNAME,  		// SMTPのユーザー名
				"password"	=> FOREIGN_MAIL_PASSWORD // SMTPのパスワード
			);

			//日本語文字化け対策
			mb_language("Japanese");
			mb_internal_encoding("UTF-8");

			//ヘッダー設定
			if (!$to && !$from)  {

				return false;
			} else {

				$header = array(
	  				"To" => $to,
	  				"From" => $from,
	  				"Subject" => mb_encode_mimeheader($subject, "UTF-8")
				);
			}
			
			//本文設定
			if ($body) {

				//smarty 設定
				$smarty = new Smarty;
				$smarty->left_delimiter		= '{[';
				$smarty->right_delimiter	= ']}';
				$smarty->compile_dir   = CB_TMP . '/smarty_mail_templates_c';


				// テンプレートの内容アサイン
				foreach ( (array) $body["data"] as $key => $val) {

					$smarty->assign($key, $val);
				}

				//tpl 設定
				$bodyContent = $smarty->fetch(CB_TPL . "/" . $body["tpl"]);

				//［文字化け対策］1行だけの長文があるか判定し分割する
				if ($this->mailLonOneLineCheck($bodyContent)) {

					$bodyContent = $this->mailLongOneLineSplit($bodyContent);
				}

				//mb_convert_encoding($bodyContent, "ISO-2022-JP", "UTF-8");
			}

			// Pear instance生成
			$mail = Mail::factory("smtp", $config);
			
			//メール配信
			if ($mail->send($to, $header, $bodyContent)) {
				return true;
			} else {
				die("");
			}
		}
	}


	/**
	*
	* メール一行文字列分割用メソッド
	*
	*/
	private function mailLongOneLineSplit($data) {

		// 一行毎の文字数制限対策
		//--------------------------------------------
		$max_byte = 450;
		$data = mb_split("\n", $data);
		foreach ((array) $data as $line => $str) {
					
			if (strlen($str) >= $max_byte) {

				$str_tmp_a = $this->mb_str_split($str, $max_byte);
				$bodyContent_tmp[$line] = implode("\n", $str_tmp_a);
				
			} else {

				$bodyContent_tmp[$line] = $str;
			}
		}

		$bodyContent = implode("\n", $bodyContent_tmp);
		return $bodyContent;
	}


	/**
	*
	* 長い文字列があるか判定
	*
	*/
	private function mailLonOneLineCheck($data) {

		$max_byte = 450;
		$data = mb_split("\n", $data);

		foreach ((array) $data as $line => $str) {
					
			if (strlen($str) >= $max_byte) {
				return true;
			}
		}
		return false;
	}


	/**
	*
	*マルチバイト文字列の分割用メソッド
	*
	*/
	private function mb_str_split($str, $split_len = 1) {

	    mb_internal_encoding('UTF-8');
	    mb_regex_encoding('UTF-8');

	    if ($split_len <= 0) {
	        $split_len = 1;
	    }

	    $strlen = mb_strlen($str, 'UTF-8');
	    $ret    = array();

	    for ($i = 0; $i < $strlen; $i += $split_len) {
	        $ret[ ] = mb_substr($str, $i, $split_len);
	    }
	    return $ret;
	}
}
