<?php
/**
 * admin
 *
 * @package
 * @subpackage
 *
 */
class NI_Action_user extends NI_Action {	

	public function auth() {

		parent::auth();
		if (isset($_COOKIE['cookie_user'])) {
			$this->session->user = unserialize($_COOKIE['cookie_user']);
		}
	
	}
	
	public function commonInit(){

		parent::commonInit();

	}

	public function checkImageFileRequire($flag){
		// フォーム有効処理（アンケート新規登録用）
		if ($_POST["submit"]["back"] == "戻る") {
			unset($_SESSION["tmp_sys_img"]);
		}
		if ($_POST["submit"]["done"] || $flag) {
			return false;
		}
		else {
			return true;
		}
	}

	private function getPostTumblr($tumblr){
		$url  ='http://api.tumblr.com/v2/blog/' . $tumblr['url'] . '/posts/?'; 
		$url .= 'api_key=' . $tumblr['api_key']; 
		$url .= '&limit=3&offset=0'; 

		$header[] = "Accept-Encoding: gzip"; 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header );
		curl_setopt($ch, CURLOPT_ENCODING , "gzip");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		$response = json_decode(curl_exec($ch));
		$i = 0;
		if($response->response->posts){
			foreach ($response->response->posts as $value) {
				$posts[$i]["title"] = $value->title;
				$posts[$i]["date"] = $value->date;
				$posts[$i]["post_url"] = $value->post_url;
				$i++;
			}
		}
		return $posts;
	}

	public function questions() {
		
		$questions = array(
				"母親の旧姓は？" => "母親の旧姓は？",
				"はじめて飼ったペットの名前は？" => "はじめて飼ったペットの名前は？",
				"小学6年生の時の先生の名前は？" => "小学6年生の時の先生の名前は？",
				"初恋の人の名前は？" => "初恋の人の名前は？",
				"通っていた幼稚園（保育園）の名前は？" => "通っていた幼稚園（保育園）の名前は？",
				"はじめて好きになったアイドルの名前は？" => "はじめて好きになったアイドルの名前は？",
			);
		
		return $questions;
	}
	
	
	public function loadImageUrl($url, $name, $path=null)
	{
		$ext = getimagesize($url);
		$ext_filename = false;
		
		switch($ext["mime"]){
	        case "image/jpeg":
	            $im = @imagecreatefromjpeg($url);
	            imagejpeg($im, $path . $name . ".jpg", 75);
	            $ext_filename = $name.'.jpg';
	        break;
	        case "image/gif":
		        $im = @imagecreatefromgif($url); 
		        imagegif($im, $path . $name . ".gif", 75);
		         $ext_filename = $name.'.gif';
			break;
			case "image/png":
			  	$im = @imagecreatefrompng($url);
				imagepng($im, $path . $name . ".png");
				 $ext_filename = $name.'.png';
			break;
		    default: 
		        $im = false;
		        $ext_filename = false;
		    break;
	    }
	    			  	
		imagedestroy($im);
	    return $ext_filename;
	}

	public function checkImageFileRequireChallengeApplyImage() {
		
		// フォーム有効処理
		if ($_POST["submit"]["back"] == "戻る") {
			unset($_SESSION["tmp_sys_img"]);
		}
		if($_POST["submit"]["confirm"] && $_FILES["challenge_apply"]["name"]['challenge_apply_image_url']) {
			
			return false;
		}
		if ($_POST["submit"]["done"]) {
			return false;
		}
		else {
			return true;
		}
	}
}
