<?php
/**
 * Formがエラーの場合クラスをerrorにするプラグイン
 * パラメーター必須：name formのエレメントのname
 * パラメーター def エラー時以外のclass name
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage smarty_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */
function smarty_function_form_error($params, &$smarty)
{
	$array = $smarty->sf->getElementErrors();
	if(empty($params['name'])){
        $smarty->_trigger_fatal_error("[plugin] parameter 'name' cannot be empty");
        return;
    }
    $array = array_unique($array);
	if($array){
		if($array[$params['name']])
		{
			$errors = $array[$params['name']];
		}
		return $errors;
	}

}