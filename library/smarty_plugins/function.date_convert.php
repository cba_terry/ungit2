<?php
/**
 * 指定日日付けに変換
 *  
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage smarty_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */

function smarty_function_date_convert($param)
{
	// 値判定
	//--------------------
	
	if (!$param["date"]) {
		return false;
	}
		
	//日付型へ変換
	//--------------------
	
	//エポック対応
	if (intval(strlen($param["date"])) == 10) {
		$date = date("Y-m-d", $param["date"]);
	} else if (intval(strlen($param["date"])) <= 2) {
		$date = date("Y-m-d", strtotime("+ " . $param["date"]));
	} else {
		$date = date("Y-m-d", $param["date"]);
	}
	
	if ($date) {
		
		return $date;
		
	}
	return false;
}

/* vim: set expandtab: */