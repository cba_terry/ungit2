<?php
/**
 * aタグがある場合、URL先を一つ前のリンク画面を挟むように書き換える
 * モバイル専用
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage smarty_plugin
 * @copyright  CYBRiDGE Nagahama
 *
 */


function smarty_modifier_external_link($string) {
		return html_entity_decode(html_entity_decode($string),ENT_COMPAT,CB_INTERNAL_ENCODE);
}