<?php
/**
 * 指定日から指定日（日付け）までを判定
 *  
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage smarty_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */

function smarty_function_date_from_to($param)
{
	// 値判定
	//--------------------
	
	if (!$param["from"] || !$param["to"]) {
		return false;
	}
		
	//日付型へ変換
	//--------------------
	
	//from
	//エポック対応
	if (intval(strlen($param["from"])) == 10) {
		$from = date("Y-m-d", $param["from"]);
	} else if (intval(strlen($param["from"])) <= 2) {
		$from = date("Y-m-d", strtotime("+ " . $param["from"]));
	} else {
		$from = date("Y-m-d", $param["from"]);
	}
	
	// to

	if (intval(strlen($param["to"])) == 10) {
		$to = date("Y-m-d", $param["to"]);
	} else if (intval(strlen($param["to"])) <= 2) {
		$to = date("Y-m-d", strtotime("+ " . $param["to"] . "day"));
	} else {
		$to = date("Y-m-d", $param["to"]);
	}
	
	//日付型を判定
	if ($from && $to) {
		
		if ($from <= $to) {
			return true;
		}
	}
	return false;
}

/* vim: set expandtab: */