<?php
/*=====================================
    PATHの定義
=====================================*/
define('CB_FRAMEWORK_PRE', 'NI');
define('CB_FW_ROOT'      , "/usr/share/php/CBFramework_3.1");
define('HTDOCS_ROOT'     , realpath(dirname(__FILE__).'/../../'));
define('CB_BASE'         , HTDOCS_ROOT);
define('CB_PUBLIC'         , CB_BASE    . '/public/');
define('CB_SMARTY_CONFIG' , CB_PUBLIC .'_configs/default.global.inc');
define('CB_TMP'          , CB_BASE . "/../tmp");
define('CB_DEBUGGING'    , 1);

define('IMAGE_TMP_FULLPATH'				, CB_PUBLIC    . '/img/sys/tmp/');
define('IMAGE_CHALLENGE_REQUEST_FULLPATH'		, CB_PUBLIC    . '/img/upload/request/');
define('IMAGE_CHALLENGE_FULLPATH'		, CB_PUBLIC    . '/img/upload/challenge/');
define('IMAGE_KEYVISUAL_FULLPATH', CB_PUBLIC .   '/img/upload/key_visual/');
define('IMAGE_MEMBER_FULLPATH'		, CB_PUBLIC    . '/img/upload/member/');
define('IMAGE_CHALLENGE_TICKET_FULLPATH'		, CB_PUBLIC    . '/img/upload/ticket/');

//開発環境の場合は、is_secure の場合でも https 接続させない
define('SSL_DISABLED'		, 1);
/*=====================================
    データベース定義
=====================================*/
define('CB_DB_TYPE' , 'pdo_mysql');
define('CB_DB_HOST' , 'localhost');
define('CB_DB_PORT' , '3306');
define('CB_DB_NAME' , 'emrld_jp');
define('CB_DB_USER' , 'root');
define('CB_DB_PASS' , 'NTao14cba');

