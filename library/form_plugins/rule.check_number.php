<?php

class CB_SecureForm_Rule_check_number extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){
		$regex = "/^\d*$/";
		if(preg_match($regex, $value)){
			
			return true;
		}
		else 
			return false;
	}
}