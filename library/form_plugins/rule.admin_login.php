<?php
/**
 * ログイン認証
 *
 *
 */
class CB_SecureForm_Rule_admin_login extends CB_QuickForm_Rule{

	function validate($value, $options) {
		$admin_name = $value[0];
		$admin_password = hash('sha512', $value[1]);

		$db_admin = CB_Factory::getDbObject("admin");

		$a_select = $db_admin->select()
					->where("admin_name = ?", $admin_name)
					->where("admin_password = ?", $admin_password)
					;
		$admin = $db_admin->fetchRow($a_select);
		if($admin)
			return true;
		else
			return false;
	}
}
