<?php

/**
 * 平仮名チェック
 * 
 * @category CB_Framework
 * @package CB_Framework
 * @subpackage form_plugin
 * @copyright CYBRiDGE
 * @license CYBRiDGE License 1.0
 */

class CB_SecureForm_Rule_check_hiragana_length extends CB_QuickForm_Rule {

	var $message = "ユーザー名は全角20文字若しくは半角英数字40文字以上で入力してください。";
	
	function validate($value, $options)
    {
        
        $length = 40;
        if (preg_match("/^[ぁ-ゞ\s]+$/u", $value)) {
            $length = 20;
        }
        if(mb_strlen($value, "UTF-8") <= $length){
            return true;
        }  
        else {
            return false;
        } 
    } 
} 
