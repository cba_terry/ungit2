<?php

class CB_SecureForm_Rule_is_array_email extends CB_QuickForm_Rule{
	
	function validate($value, $options = null) {
		
		$db_member = CB_Factory :: getDbObject("member");
		
		$email = $db_member->isEmailFacebookExists($value);
		if(!empty($email["member_id"]))
			return false;
		else 
			return true;
	}
}