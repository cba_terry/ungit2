<?php

class CB_SecureForm_Rule_check_current_pass extends CB_QuickForm_Rule{
	function validate($value, $options = NULL){
		$db_member = CB_Factory :: getDbObject("member");

			$value[0] = hash("sha512", $value[0]);
			
			if($db_member->isMemberPassword($value[0], $options)){
				return true; 
			} 
			else {
				return false;
			}
				
   }
}
