<?php

class CB_SecureForm_Rule_password_min_max extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){

		if( strlen($value[0]) < 8 || strlen($value[0]) > 20)
			return false;
		else 
			return true;
	}
}