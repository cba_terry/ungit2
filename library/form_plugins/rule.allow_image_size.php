<?php
/**
 * エラーメッセージを表示させるためのダミールール
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage form_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */

class CB_SecureForm_Rule_allow_image_size extends CB_QuickForm_Rule
{
	function validate($value, $options = null){
		
		if($value["error"] == 1) {
			return false;
		} else {
			return true;
		}
	}
}