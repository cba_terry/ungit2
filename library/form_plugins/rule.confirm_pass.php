<?php

class CB_SecureForm_Rule_confirm_pass extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){
		$member_pass = $value[0];
		$confirm_member_pass = $value[1];
				
		if(strcmp($member_pass, $confirm_member_pass) == 0)
			return true;
		else 
			return false;
	}
}