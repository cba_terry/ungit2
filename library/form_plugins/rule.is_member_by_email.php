<?php

class CB_SecureForm_Rule_is_member_by_email extends CB_QuickForm_Rule{
	function validate($value, $options){
		$db_member = CB_Factory :: getDbObject("member");

		if($db_member->isMemberByEmail($value)){
			return true; 
		} 
		else {
			return false;
		}
				
   }
}
