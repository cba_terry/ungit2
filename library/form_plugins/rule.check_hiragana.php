<?php

/**
 * 平仮名チェック
 * 
 * @category CB_Framework
 * @package CB_Framework
 * @subpackage form_plugin
 * @copyright CYBRiDGE
 * @license CYBRiDGE License 1.0
 */

class CB_SecureForm_Rule_check_hiragana extends CB_QuickForm_Rule {

	var $message = "[name]はひらがなで入力してください。";
	
	function validate($value, $options)
    {
        if (preg_match("/^[ぁ-ゞ\s]+$/u", $value)) {
            return true;
        } else {
            return false;
        } 
    } 
} 
