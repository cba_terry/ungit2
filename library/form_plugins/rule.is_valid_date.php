<?php
/**
 * 存在する日付かどうかを判定する
 *
 * @category CB_Framework
 * @package CB_Framework
 * @subpackage form_plugin
 * @copyright CYBRiDGE
 * @license CYBRiDGE License 1.0
 */

class CB_SecureForm_Rule_default extends CB_QuickForm_Rule
{
	function validate($value, $options)
    {
		$checkdate_flag = checkdate($value["m"], $value["d"], $value["Y"]);
		return $checkdate_flag;
   }
}