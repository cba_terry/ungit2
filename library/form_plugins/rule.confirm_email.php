<?php

class CB_SecureForm_Rule_confirm_email extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){
		$member_email = $value[1];
		$confirm_member_email = $value[0];
		
		if(strcmp($member_email, $confirm_member_email) != 0)
			return false;
		else 
			return true;
	}
	
}