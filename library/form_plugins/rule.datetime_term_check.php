<?php

//開始・終了時間有効性チェック

class CB_SecureForm_Rule_datetime_term_check extends CB_QuickForm_Rule{
	
	function validate($value, $options=null){

		$start_datetime_arr = $value[0];
		$end_datetime_arr = $value[1];
		
		$start_datetime = $start_datetime_arr["Y"]."-".$start_datetime_arr["m"]."-".$start_datetime_arr["d"]." ".$start_datetime_arr["H"].":".$start_datetime_arr["i"];
		$end_datetime = $end_datetime_arr["Y"]."-".$end_datetime_arr["m"]."-".$end_datetime_arr["d"]." ".$end_datetime_arr["H"].":".$end_datetime_arr["i"];
		
		if (strtotime($start_datetime) < strtotime($end_datetime)){
			return true;
		}
		return false;
   }
}
