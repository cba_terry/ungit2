<?php
/**
 * メールアドレスとパスワードの妥当性チェック
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage form_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */


class CB_SecureForm_Rule_integer extends CB_QuickForm_Rule
{
    function validate($value, $options = null)
    {
		return preg_match("/^(|-)[0-9]+$/", $value);
    }
}
