<?php

class CB_SecureForm_Rule_check_question extends CB_QuickForm_Rule{
	
	function validate($value, $options = null) {
		
		$db_member = CB_Factory :: getDbObject("member");
		
		$question = $db_member->isQuestionExists($value);
		
		if(!empty($question))
			return false;
		else 
			return true;
	}
}