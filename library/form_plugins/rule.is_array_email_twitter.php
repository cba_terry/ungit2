<?php

class CB_SecureForm_Rule_is_array_email_twitter extends CB_QuickForm_Rule{
	
	function validate($value, $options = null) {
		
		$db_member = CB_Factory :: getDbObject("member");
		
		$email = $db_member->isEmailTwitterExists($value);
		
		if(!empty($email["member_id"]))
			return false;
		else 
			return true;
	}

}